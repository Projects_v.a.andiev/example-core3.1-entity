﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Pages;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.ViewModel.SearchByLastName
{
    public class ViewSearchUsersModel
    {
        public PagedList<ViewUserChinModel> PagedListUser { get; set; }

    }
}
