﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Infrastructure.CustomAttribute.Users;
using webQueueUsersDistrict.Models;
using webQueueUsersDistrict.Models.Interfaces;

namespace webQueueUsersDistrict.ViewModel.Update
{
    public class ViewUserModel
    {
        public int UserId { get; set; }

        [ValidationAttributeLogin]
        [Required(ErrorMessage = "Введите Логин")]
        [Display(Name = "Логин")]
        public virtual string Login { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        public bool StatusUser { get; set; }
    }
}
