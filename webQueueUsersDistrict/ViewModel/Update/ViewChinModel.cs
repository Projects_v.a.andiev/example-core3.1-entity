﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Infrastructure;

namespace webQueueUsersDistrict.ViewModel.Update
{
    public class ViewChinModel
    {
        public int? Kod { get; set; }
        [Required(ErrorMessage = "Требуется ввести должность")]
        [Display(Name = "Должность")]
        public string Dolgn { get; set; }

        [CustomValidationAttributeLocation(ErrorMessage = "Требуется ввести № кабинета")]
        [Required(ErrorMessage = "Требуется ввести № кабинета")]
        [Range(0, short.MaxValue,ErrorMessage ="Можно вводить только числа!!")]       
        public short? Pomesh { get; set; }
      
        [Required(ErrorMessage = "Требуется ввести приемные дни")]
        [Display(Name = "Кабинет")]
        public string PriemDay { get; set; }

        
        [Required (ErrorMessage="Требуется ввести номер телефона")]
        [StringLength(10,  ErrorMessage = "Длина телефона от 1 до 10 символов")]
        [RegularExpression(@"^[0-9\-\/]+$", ErrorMessage = "Можно вводить только числа!")]
        public string Telef { get; set; }

        [Required(ErrorMessage = "Требуется ввести фамилию")]
        [Display(Name = "Фамилия")]
        public string Fam { get; set; }

        [Required(ErrorMessage = "Требуется ввести имя")]
        [Display(Name = "Имя")]
        public string Im { get; set; }

        [Required(ErrorMessage = "Требуется ввести отчество")]
        [Display(Name = "Отчество")]
        public string Otch { get; set; }
        
    }

   
}
