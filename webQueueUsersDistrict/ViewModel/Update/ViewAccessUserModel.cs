﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.ViewModel.Update
{
    public class ViewAccessUserModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool AccessDistrict { get; set; }
        public int DistrictId { get; set; }

        public string DistrictName {get; set;}
    }
}
