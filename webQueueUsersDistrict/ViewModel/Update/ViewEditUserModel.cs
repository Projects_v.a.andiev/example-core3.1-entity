﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.ViewModel.Update
{
    public class ViewEditUserModel
    {
        public ViewUserModel User { get; set; }
        public ViewChinModel Chin { get; set; }
        public List<ViewAccessUserModel> AccessUserToDistricts { get; set; }
        public List<ViewUserRolesModel> UserRolesModel { get; set; }
        public List<ViewRolesModel> RolesModel { get; set; }

        public List<ViewDistictsModel> DistictsModel { get; set; }

    }
}
