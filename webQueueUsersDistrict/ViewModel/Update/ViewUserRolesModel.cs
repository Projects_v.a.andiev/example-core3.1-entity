﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.ViewModel.Update
{
    public class ViewUserRolesModel
    {
        public int N { get; set; }
        public int Users { get; set; }
        public int Role { get; set; }
        public bool Access { get; set; }
        public string RoleName { get; set; }
}
}
