﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.ViewModel.Update
{
    public class ViewDistictsModel
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
    }
}
