﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webQueueUsersDistrict.Models;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.Models.UsersOperation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using webQueueUsersDistrict.ViewModel.Update;
using webQueueUsersDistrict.Models.UsersOperation.SearchUsers;
using webQueueUsersDistrict.ViewModel.SearchByLastName;
using webQueueUsersDistrict.Models.Pages;
using webQueueUsersDistrict.Models.UsersOperation.AddUser;

namespace webQueueUsersDistrict.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        IRepositoryModel _repository;
        ViewEditUserModel _updateModel;
        EditUser _editUser;
        UpdateInsertUsers _updateInsertUsers;

        ISearchUserRepositoryModel _searchUserRepositoryModel;
        ViewSearchUsersModel _viewSearchUsersModel;
        CreateUser _createUser;

        public HomeController(ILogger<HomeController> logger, IRepositoryModel repository, ISearchUserRepositoryModel searchUserRepositoryModel)
        {
            var updateModel = new ViewEditUserModel();
            _logger = logger;
            _repository = repository;
            _updateModel = updateModel;
            var editUser = new EditUser(_repository);
            _editUser = editUser;

            var updateInsertUsers = new UpdateInsertUsers(_repository);
            _updateInsertUsers = updateInsertUsers;

            _searchUserRepositoryModel = searchUserRepositoryModel;
            _viewSearchUsersModel = new ViewSearchUsersModel();

            _createUser = new CreateUser(repository);

        }

        public IActionResult Index()
        {
            return View(_repository.UserRolesFull());
        }

        [Authorize(Policy = "DCUsers")]
        [Authorize(Policy = "NotBob")]
        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Search()
        {

            return View("SearchUsers");
        }

        [HttpPost]
        public IActionResult SearchUsers(QueryOptions options, ViewSearchUsersModel test)
        {

            var users = _searchUserRepositoryModel.ChinByPage();

            var pagedUsers = _searchUserRepositoryModel.FeedViewChinsModel(users, options);



            _viewSearchUsersModel.PagedListUser = _searchUserRepositoryModel.FeedViewChinsModel(pagedUsers, options);

            return View(_viewSearchUsersModel);

        }

        [HttpGet]
        public IActionResult SearchUsers(string lastName, QueryOptions options, ViewSearchUsersModel test)
        {
            if (!string.IsNullOrWhiteSpace(lastName) || !string.IsNullOrEmpty(lastName))
            {
                var users = _searchUserRepositoryModel.SearchByLastName(lastName);

                var pagedUsers = _searchUserRepositoryModel.FeedViewChinsModel(users, options);

                _viewSearchUsersModel.PagedListUser = _searchUserRepositoryModel.FeedViewChinsModel(pagedUsers, options);

                return View(_viewSearchUsersModel);
            }
            else
            {
                return Redirect(Request.Headers["Referer"].ToString());
            }
        }

        [HttpGet]
        public IActionResult EditUserByLastName(string lastName)
        {
            if (!string.IsNullOrWhiteSpace(lastName) || !string.IsNullOrEmpty(lastName))
            {
                var chin = _editUser.ChinFromSecondName(lastName);

                var user = _editUser.userDistrict(chin.UsersDistrict.FirstOrDefault().UserId);

                _updateModel.User = _editUser.FeedUpdateUserModel(user);

                _updateModel.Chin = _editUser.FeedUpdateChinModel(chin);

                _updateModel.RolesModel = _editUser.FeedRolesModel();

                _updateModel.UserRolesModel = _editUser.AllRoles(_updateModel.User, _updateModel.RolesModel, _repository.Roles);

                _updateModel.DistictsModel = _editUser.FeedDistrictsModel();

                _updateModel.AccessUserToDistricts = _editUser.AllAccessUserDistricts(_updateModel.User, _updateModel.DistictsModel, _repository.Districts);

                return View(_updateModel);
            }
            else
            {
                return Redirect(Request.Headers["Referer"].ToString());
            }
        }


        [HttpPost]
        public IActionResult DeleteUser(int id)
        {
            return View("SearchUsers");
        }

        [HttpGet]
        public IActionResult EditUserById(int id)
        {
            if (id > 0)
            {
                var user = _editUser.userDistrict(id);
                var chin = _editUser.ChinById(user.KodChin.GetValueOrDefault());

                _updateModel.User = _editUser.FeedUpdateUserModel(user);

                _updateModel.Chin = _editUser.FeedUpdateChinModel(chin);

                _updateModel.RolesModel = _editUser.FeedRolesModel();

                _updateModel.UserRolesModel = _editUser.AllRoles(_updateModel.User, _updateModel.RolesModel, _repository.Roles);

                _updateModel.DistictsModel = _editUser.FeedDistrictsModel();

                _updateModel.AccessUserToDistricts = _editUser.AllAccessUserDistricts(_updateModel.User, _updateModel.DistictsModel, _repository.Districts);

                return View("EditUser", _updateModel);
            }
            else
            {
                return Redirect(Request.Headers["Referer"].ToString());
            }
        }

        [HttpGet]
        public IActionResult CreateUser()
        {
            ViewEditUserModel model = _createUser.NewUserModel();
            model.DistictsModel = _editUser.FeedDistrictsModel();
            model.RolesModel = _editUser.FeedRolesModel();
            model.UserRolesModel = _editUser.AllRoles(model.User, model.RolesModel, _repository.Roles);
            model.AccessUserToDistricts = _editUser.AllAccessUserDistricts(model.User, model.DistictsModel, _repository.Districts);

            return View("EditUser", model);
        }

        [HttpPost]
      
        public IActionResult EditUser(ViewEditUserModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                model.User.Login = _updateInsertUsers.CodingValue(model.User.Login);
                model.User.Password = _updateInsertUsers.CodingValue(model.User.Password);
                var newModel = _updateInsertUsers.Parse(model);
                return View("EditUser", newModel);
            }
            else
            {
                //Что-то не так со значениями данных
                return View("EditUser", model);
            }

            //return RedirectToAction("AdministrationUsers");
        }


        [HttpGet]

        public IActionResult TestCodingValue()
        {
            var codingLogin = _updateInsertUsers.CodingValue("ФИО ИНСПЕКТОРА");
            return View("");
        }


            public IActionResult AdministrationUsers()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }

            return RedirectToAction("");
        }

        [HttpPost]
        public IActionResult UpdateInsertUser(ViewEditUserModel model)
        {
            if (ModelState.IsValid)
            {
                model.User.Login = _updateInsertUsers.CodingValue(model.User.Login);
                model.User.Password = _updateInsertUsers.CodingValue(model.User.Password);
                _updateInsertUsers.Parse(model);
                return Redirect(Request.Headers["Referer"].ToString());
            }
            else
            {
                //Что-то не так со значениями данных
                return View(model);
            }
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
