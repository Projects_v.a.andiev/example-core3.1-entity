﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.Models.Pages;
using webQueueUsersDistrict.ViewModel.SearchByLastName;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Models
{
    public class RepositoryModel : IRepositoryModel
    {
        private UsersContext _context;
        public RepositoryModel(UsersContext context)
        {
            _context = context;
        }

        public string CodingValue(string value)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                var param = new SqlParameter("@Str", value);
                command.CommandText = "SELECT  dbo.Kodirovanie (@Str)";
                command.Parameters.Add(param);
                _context.Database.OpenConnection();
                object result = command.ExecuteScalar();
                if (result != null)
                {
                    return result.ToString();
                }
                throw new Exception("Ошибка выполенения хранимой процедуры Kodirovanie!");
            }
        }

        public List<UserRoles> UserRolesList
        {
            get
            {
                return
                _context.UserRoles.ToList();
                //.Include(userRoles => userRoles.RoleNavigation)
                ////.Where(x => x.UsersNavigation == )
                //.Include(userInfo1 => userInfo1.UsersNavigation.AccessUserToDistrict)
                //.Include(userInfo1 => userInfo1.UsersNavigation.KodChinNavigation)
                //.ToList();
            }
        }

        public List<UserRoles> UserRolesFull()
        {
            return
                _context.UserRoles
                .Include(userRoles => userRoles.RoleNavigation)
                //.Where(x => x.UsersNavigation == )
                .Include(userInfo1 => userInfo1.UsersNavigation.AccessUserToDistrict)
                .Include(userInfo1 => userInfo1.UsersNavigation.KodChinNavigation).Take(10)
                .ToList();
        }


        public List<UsersDistrict> UsersDistrictById(int id)
        {
            return
               _context
               .UsersDistrict
               .Select(s => new UsersDistrict
               {
                   KodChin = s.KodChin,
                   Login = SPCodingDecodingValue.DeCodingValue(s.Login),
                   Password = SPCodingDecodingValue.DeCodingValue(s.Password),
                   StatusUser = s.StatusUser,
                   MsNetVersion = s.MsNetVersion,
                   UserId = s.UserId,                   
               })
               .Where(w => w.UserId == id)
               .ToList();
        }

        public UsersDistrict UsersDistrictByLogin(string mail)
        {
            return
               _context
               .UsersDistrict
                .Select(s => new UsersDistrict
                {
                    KodChin = s.KodChin,
                    Login = SPCodingDecodingValue.DeCodingValue(s.Login),
                    Password = SPCodingDecodingValue.DeCodingValue(s.Password),
                    StatusUser = s.StatusUser,
                    MsNetVersion = s.MsNetVersion,
                    UserId = s.UserId
                })
               .Where(w => w.Login == mail)
               .FirstOrDefault();
        }

        public List<Role> Roles
        {
            get
            {
                return
                   _context.Role
                       .ToList();
            }
        }

        public List<Spr02> Districts
        {

            get
            {
                var districts = new List<int>() { };

                for (int i = 1; i < 19; i++)
                {
                    districts.Add(i);
                }

                return
                   _context
                   .Spr02
                   .Where(w => districts.Select(x => x)
                   .Contains(w.Kod))
                   .ToList();
            }
        }

        public List<AccessUserToDistrict> AccessUserToDistrictsByUserId(int id)
        {

            return
               _context
               .AccessUserToDistrict
               .Where(w => w.UserId == id)
                   .Include(u => u.AccessUserToDistrictSpr02Navigation)
                   .OrderBy(o => o.DistrictId)
                   .ToList();

        }

        public List<Chin> ChinsById(int id)
        {
            return
               _context
               .Chin
               .Where(w => w.Kod == id)
               .Include(u => u.UsersDistrict)
               .ToList();
        }


        public List<Chin> ChinsByLastName(string lastName)
        {
            var chins = _context
                .Chin
                .Where(w => EF.Functions.Like(w.Fam, $@"%{lastName}%")).ToList();

            var users = _context
               .Chin
               .Include(x => x.UsersDistrict)
               .Where(w => chins.Select(x => x.Kod).Contains(w.Kod)).ToList();

            return users;
        }


        //public PagedList<Chin> ChinsByLastName(string lastName, QueryOptions options)
        //{
        //    return new PagedList<Chin>(ChinsByLastName(lastName), options);
        //}

        public void UpdateChin(ViewChinModel updateChinModel)
        {

            var chin = _context.Chin.Where(x => x.Kod == updateChinModel.Kod).FirstOrDefault();

            chin.Dolgn = updateChinModel.Dolgn;
            chin.Fam = updateChinModel.Fam;
            chin.Im = updateChinModel.Im;
            chin.Otch = updateChinModel.Otch;
            chin.Pomesh = updateChinModel.Pomesh;
            chin.PriemDay = updateChinModel.PriemDay;
            chin.Telef = updateChinModel.Telef;

            _context.Chin.Update(chin);
            _context.SaveChanges();
        }

        public void UpdateUsers(ViewUserModel updateUserModel)
        {

            var user = _context.UsersDistrict.Where(x => x.UserId == updateUserModel.UserId).FirstOrDefault();

            user.Login = updateUserModel.Login;
            user.Password = updateUserModel.Password;
            user.StatusUser = Convert.ToInt32(updateUserModel.StatusUser);

            _context.UsersDistrict.Update(user);
            _context.SaveChanges();
        }

        public UsersDistrict AddNewUsers(ViewEditUserModel newUserModel)
        {
            var newUser = new UsersDistrict();

            newUser.Login = newUserModel.User.Login;
            newUser.Password = newUserModel.User.Password;
            newUser.StatusUser = Convert.ToInt32(newUserModel.User.StatusUser);
            newUser.KodChinNavigation = new Chin
            {
                Dolgn = newUserModel.Chin.Dolgn,
                Fam = newUserModel.Chin.Fam,
                Im = newUserModel.Chin.Im,
                Otch = newUserModel.Chin.Otch,
                Pomesh = newUserModel.Chin.Pomesh,
                PriemDay = newUserModel.Chin.PriemDay,
                Telef = newUserModel.Chin.Telef
            };
            newUser.UserRoles = GetNewRoles(newUserModel);
            newUser.AccessUserToDistrict = GetAccessUser(newUserModel);

            _context.UsersDistrict.AddRange(newUser);
            _context.SaveChanges();

            return newUser;
        }

        public List<AccessUserToDistrict> GetAccessUser(ViewEditUserModel viewEditUserModel)
        {
            var accesses = new List<AccessUserToDistrict>();
            foreach (var access in viewEditUserModel.AccessUserToDistricts)
            {
                var newAccess = new AccessUserToDistrict()
                {
                    AccessDistrict = Convert.ToInt32(access.AccessDistrict),
                    DistrictId = access.DistrictId
                };
                accesses.Add(newAccess);
            }
            return accesses;
        }

        public List<UserRoles> GetNewRoles(ViewEditUserModel viewEditUserModel)
        {
            var roles = new List<UserRoles>();
            foreach (var role in viewEditUserModel.UserRolesModel)
            {
                if (role.Access)
                {
                    var r = new UserRoles()
                    {
                        Role = role.Role
                    };
                    roles.Add(r);
                }
            }
            return roles;
        }

        public void AddRoleUser(List<UserRoles> updateRoles)
        {
            _context.UserRoles.AddRange(updateRoles);
            _context.SaveChanges();
        }

        public void DeleteRoleUser(List<UserRoles> deleteRoles)
        {

            _context.UserRoles.RemoveRange(deleteRoles);
            _context.SaveChanges();
        }


        public List<AccessUserToDistrict> UpdatedRecords(List<ViewAccessUserModel> updateAccess)
        {
            return _context.AccessUserToDistrict.Where(w => updateAccess.Select(s => s.UserId).Contains(w.UserId)).ToList();


        }

        public void UpdateAccessUser(List<AccessUserToDistrict> changeAccess, List<ViewAccessUserModel> updateAccess)
        {
            foreach (var change in changeAccess)
            {
                foreach (var newValue in updateAccess)
                {
                    if (change.UserId == newValue.UserId && change.DistrictId == newValue.DistrictId)
                    {
                        change.AccessDistrict = Convert.ToInt32(newValue.AccessDistrict);
                        _context.UpdateRange(change);
                    }
                }
            }
            _context.SaveChanges();
        }

        public void AddAccessUser(List<ViewAccessUserModel> missingAccessValue, List<ViewAccessUserModel> accessUserToDistricts)
        {
            var listAccess = new List<AccessUserToDistrict>();
            foreach (var access in missingAccessValue)
            {
                foreach (var newAccessValue in accessUserToDistricts)
                {
                    if (newAccessValue.DistrictId == access.DistrictId && access.UserId == newAccessValue.UserId)
                    {
                        var newValue = new AccessUserToDistrict()
                        {
                            AccessDistrict = newAccessValue.AccessDistrict ? 1 : 0,
                            DistrictId = newAccessValue.DistrictId,
                            UserId = newAccessValue.UserId,
                            User = _context.UsersDistrict.Where(w => w.UserId == newAccessValue.UserId).FirstOrDefault(),
                            DateCreate = DateTime.Now,
                        };
                        listAccess.Add(newValue);
                    }
                }
            }
            _context.AccessUserToDistrict.AddRange(listAccess);
            _context.SaveChanges();
        }

        //public void RemoveAccessUser(List<ViewAccessUserModel> updateAccess)
        //{

        //    foreach (var access in updateAccess.Where(x => x.AccessDistrict == false))
        //    {
        //        var newRole = new AccessUserToDistrict()
        //        {
        //            AccessDistrict = Convert.ToInt32(access.AccessDistrict),
        //            DistrictId = access.DistrictId,
        //            UserId = access.UserId,
        //            Id = access.Id,

        //        };

        //        _context.Entry(newRole)
        //        .Property(c => c.AccessDistrict).IsModified = true;
        //    }

        //    _context.SaveChanges();
        //}


        public void DeleteUserById(int userId)
        {
            _context
                .UsersDistrict
                .Remove(_context.UsersDistrict.Find(userId));

        }


    }
}
