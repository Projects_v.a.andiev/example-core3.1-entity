﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using webQueueUsersDistrict.Helpers;

namespace webQueueUsersDistrict.Models.Pages
{
    public class PagedList<T> : List<T>
    {
        DivideHelper _divideHelper;
        public PagedList(IQueryable<T> query, QueryOptions options = null)
        {
            _divideHelper = new DivideHelper();
            CurrentPage = options.CurrentPage;
            PageSize = options.PageSize;
            Options = options;
            if (options != null)
            {
                if (!string.IsNullOrEmpty(options.OrderPropertyName))
                {
                    query = Order(query, options.OrderPropertyName,
                    options.DescendingOrder);
                }

                if (!string.IsNullOrEmpty(options.SearchPropertyName)
                && !string.IsNullOrEmpty(options.SearchTerm))
                {
                    query = Search(query, options.SearchPropertyName,
                    options.SearchTerm);
                }
            }

            TotalPages = _divideHelper.DivRoundUp(query.Count(), PageSize);
            AddRange(query.Skip((CurrentPage - 1) * PageSize).Take(PageSize));

        }

        public PagedList(List<T> query, int currentPage, int pageSize, int totalPages, QueryOptions options = null)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            Options = options;
            AddRange(query);
        }

        private static IQueryable<T> Search(IQueryable<T> query, string propertyName, string searchTerm)
        {
            var parametr = Expression.Parameter(typeof(T), "x");
            var source = propertyName.Split('.').Aggregate((Expression)parametr, Expression.Property);
            var body = Expression.Call(source, "Contains", Type.EmptyTypes, Expression.Constant(searchTerm, typeof(string)));
            var lambda = Expression.Lambda<Func<T, bool>>(body, parametr);
            return query.Where(lambda);
        }

        private static IQueryable<T> Order(IQueryable<T> query, string propertyName, bool desc)
        {
            var parametr = Expression.Parameter(typeof(T), "x");
            var source = propertyName.Split('.').Aggregate((Expression)parametr, Expression.Property);
            var lambda = Expression.Lambda(typeof(Func<,>).MakeGenericType(typeof(T), source.Type), source, parametr);

            return typeof(Queryable).GetMethods().Single(method => method.Name == (desc ? "OrderByDescending" : "OrderBy")
            && method.IsGenericMethodDefinition
            && method.GetGenericArguments().Length == 2
            && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(T), source.Type)
                .Invoke(null, new object[] { query, lambda }) as IQueryable<T>;
        }



        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

        public int TotalPages { get; set; }

        public QueryOptions Options { get; set; }

        public bool HasPreviousPage => CurrentPage > 1;
        public bool HasNextPage => CurrentPage < TotalPages;
    }
}
