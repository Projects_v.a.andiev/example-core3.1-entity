﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Models.UsersOperation.AddUser
{
    public class CreateUser
    {
        private IRepositoryModel repository;

        public CreateUser(IRepositoryModel repository)
        {
            this.repository = repository;
        }

        public ViewEditUserModel NewUserModel()
        {
            ViewEditUserModel model = new ViewEditUserModel();
            model.Chin = new ViewChinModel();
            model.User = new ViewUserModel();
            return model;
        }
    }
}
