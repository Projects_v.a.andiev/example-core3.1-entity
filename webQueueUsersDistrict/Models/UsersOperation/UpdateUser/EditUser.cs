﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Models.UsersOperation
{
    public class EditUser
    {
        IRepositoryModel _repository;
        public EditUser(IRepositoryModel repository)
        {
            _repository = repository;
        }
        public Chin ChinFromSecondName(string secondName)
        {
            return _repository.ChinsByLastName(secondName).FirstOrDefault();
        }

        public Chin ChinById(int id)
        {
            return _repository.ChinsById( id).FirstOrDefault();
        }

        public UsersDistrict userDistrict(int userId)
        {
            return _repository.UsersDistrictById(userId).FirstOrDefault();

        }

        public ViewUserModel FeedUpdateUserModel(UsersDistrict usersDistrict)
        {
            var userModel = new ViewUserModel();
            userModel.UserId = usersDistrict.UserId;
            userModel.Login = usersDistrict.Login;
            userModel.Password = usersDistrict.Password;
            userModel.StatusUser = Convert.ToBoolean(usersDistrict.StatusUser);
            return userModel;
        }

        public ViewChinModel FeedUpdateChinModel(Chin chin)
        {
            var chinModel = new ViewChinModel();
            chinModel.Kod = chin.Kod;
            chinModel.Fam = chin.Fam;
            chinModel.Im = chin.Im;
            chinModel.Otch = chin.Otch;
            chinModel.Pomesh = chin.Pomesh;
            chinModel.PriemDay = chin.PriemDay;
            chinModel.Telef = chin.Telef;
            chinModel.Dolgn = chin.Dolgn;
            return chinModel;
        }

        public ViewUserModel FeedUpdateChinModel(UsersDistrict usersDistrict)
        {
            var userModel = new ViewUserModel();
            userModel.UserId = usersDistrict.UserId;
            userModel.Login = usersDistrict.Login;
            userModel.Password = usersDistrict.Password;
            userModel.StatusUser = Convert.ToBoolean(usersDistrict.StatusUser);           
            return userModel;
        }

        public List<ViewRolesModel> FeedRolesModel()
        {
            var rolesModel = new List<ViewRolesModel>();
            foreach (var role in _repository.Roles)
            {
                var roleModel = new ViewRolesModel()
                {
                    Id = role.Id,
                    Role1 = role.Role1
                };
                rolesModel.Add(roleModel);
            }

            return rolesModel;
        }

        public List<ViewDistictsModel> FeedDistrictsModel()
        {
            var districtsModel = new List<ViewDistictsModel>();
            foreach (var access in _repository.Districts)
            {
                var districtModel = new ViewDistictsModel()
                {
                    DistrictId = access.Kod,
                    DistrictName = access.FullName
                };
                districtsModel.Add(districtModel);
            }

            return districtsModel;
        }
        

        public List<ViewUserRolesModel> ExistingRoles(ViewUserModel updateUserModel, List<ViewRolesModel> rolesModels)
        {
            var currentUserRoles = new List<ViewUserRolesModel>();
            foreach (var role in _repository.UserRolesList.Where(w => w.Users == updateUserModel.UserId))
            {
                var roleModel = new ViewUserRolesModel()
                {
                    N = role.N,
                    Role = role.Role,
                    Users = role.Users,
                    Access = true,
                    RoleName = rolesModels.Where(w => w.Id == role.Role).FirstOrDefault().Role1
                };
                currentUserRoles.Add(roleModel);
            }
            return currentUserRoles;
        }



        public List<ViewUserRolesModel> UnavailableRoles(ViewUserModel updateUserModel, List<ViewUserRolesModel> existingRoles, List<Role> roles)
        {
            var emptyUserRoles = new List<ViewUserRolesModel>();
            foreach (var role in roles.Where(w => !existingRoles.Select(x => x.Role).Contains(w.Id)).ToList())
            {
                var roleModel = new ViewUserRolesModel()
                {
                    Role = role.Id,
                    Users = updateUserModel.UserId,
                    Access = false,
                    RoleName = roles.Where(w => w.Id == role.Id).FirstOrDefault().Role1
                };
                emptyUserRoles.Add(roleModel);
            }

            return emptyUserRoles;
        }

        public List<ViewAccessUserModel> ExistingAccessUserToDistricts(ViewUserModel updateUserModel)
        {
            var accessUserToDistricts = new List<ViewAccessUserModel>();
            foreach (var access in _repository.AccessUserToDistrictsByUserId(updateUserModel.UserId))
            {
                var updateAccessUserModel = new ViewAccessUserModel()
                {
                    AccessDistrict = Convert.ToBoolean(access.AccessDistrict),
                    DistrictId = access.DistrictId,
                    UserId = access.UserId,
                    Id = access.Id,
                    DistrictName = access.AccessUserToDistrictSpr02Navigation.FullName
                };
                accessUserToDistricts.Add(updateAccessUserModel);
            }

            return accessUserToDistricts;
        }

        public List<ViewAccessUserModel> UnavailableAccessUserToDistricts(ViewUserModel updateUserModel, List<ViewAccessUserModel> existingAccesses, List<Spr02> districts)
        {
            var accessUserToDistricts = new List<ViewAccessUserModel>();
            //foreach (var role in roles.Where(w => !existingRoles.Select(x => x.Role).Contains(w.Id)).ToList())
            foreach (var access in districts.Where( w => !existingAccesses.Select(x => x.DistrictId).Contains(w.Kod)).ToList())
            {
                var updateAccessUserModel = new ViewAccessUserModel()
                {
                    AccessDistrict = false,
                    DistrictId = access.Kod,
                    UserId = updateUserModel.UserId,
                    Id = 0,
                    DistrictName = access.FullName
                };
                accessUserToDistricts.Add(updateAccessUserModel);
            }

            return accessUserToDistricts;
        }

        private List<ViewAccessUserModel> UnionUserAccess(List<ViewAccessUserModel> AccessUserToDistricts, List<ViewAccessUserModel> existingAccessUserToDistricts)
        {
            AccessUserToDistricts.AddRange(existingAccessUserToDistricts);

            return AccessUserToDistricts.OrderBy(x => x.DistrictId).ToList();
        }

        public List<ViewAccessUserModel> AllAccessUserDistricts(ViewUserModel updateUserModel, List<ViewDistictsModel> rolesModels, List<Spr02> roles)
        {
            var existingAccess = ExistingAccessUserToDistricts(updateUserModel);
            return UnionUserAccess(UnavailableAccessUserToDistricts(updateUserModel, existingAccess, roles), existingAccess);
        }


        private List<ViewUserRolesModel> UnionUserRoles(List<ViewUserRolesModel> noRoles, List<ViewUserRolesModel> existingRoles)
        {
            noRoles.AddRange(existingRoles);

            return noRoles.OrderBy(x => x.Role).ToList();
        }

        public List<ViewUserRolesModel> AllRoles(ViewUserModel updateUserModel, List<ViewRolesModel> rolesModels, List<Role> roles)
        {
            var existingRole = ExistingRoles(updateUserModel, rolesModels);
            return UnionUserRoles(UnavailableRoles(updateUserModel, existingRole, roles), existingRole);
        }


    }
}
