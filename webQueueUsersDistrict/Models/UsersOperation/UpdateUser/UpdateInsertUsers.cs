﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Models.UsersOperation
{
    public class UpdateInsertUsers
    {
        IRepositoryModel _repository;
        EditUser _editUser;
        public UpdateInsertUsers(IRepositoryModel repository)
        {
            _repository = repository;
            _editUser = new EditUser(repository);
        }

        public string CodingValue(string value)
        {
            return _repository.CodingValue(value);
        }

        public ViewEditUserModel Parse(ViewEditUserModel model)
        {
            if (model.User.UserId != 0)
            {

                _repository.UpdateUsers(model.User);
                _repository.UpdateChin(model.Chin);
                _repository.AddRoleUser(GetNewRoles(model.UserRolesModel));
                _repository.DeleteRoleUser(WhichRolesRemove(model.UserRolesModel));
                _repository.UpdateAccessUser(_repository.UpdatedRecords(model.AccessUserToDistricts), model.AccessUserToDistricts);

                var missingAccessValue = _editUser.UnavailableAccessUserToDistricts(model.User, _editUser.ExistingAccessUserToDistricts(model.User), _repository.Districts);
                _repository.AddAccessUser(missingAccessValue, model.AccessUserToDistricts);
                //_repository.RemoveAccessUser(model.AccessUserToDistricts);
                return model;
            }
            else
            {
                var newUser = _repository.AddNewUsers(model);
                model.User.UserId = newUser.UserId;
                model.Chin.Kod = newUser.KodChin;
                
                foreach (var item in model.AccessUserToDistricts)
                {
                    item.UserId = newUser.UserId;
                }
                foreach (var item in model.UserRolesModel)
                {
                    item.Users = newUser.UserId;
                }
                return model;
            }
        }


        public List<UserRoles> GetNewRoles(List<ViewUserRolesModel> roles)
        {
            var listRole = new List<UserRoles>();
            var dbRoles = _repository.UserRolesList.ToList();
            var chekedRoles = roles.Where(w => !dbRoles.Select(x => x.N).Contains(w.N) && w.Access == true).ToList();


            foreach (var role in chekedRoles)
            {
                var newRole = new UserRoles()
                {
                    Role = role.Role,
                    Users = role.Users,

                };
                listRole.Add(newRole);
            }
            return listRole;
        }


        public List<UserRoles> WhichRolesRemove(List<ViewUserRolesModel> roles)
        {
            var dbRoles = _repository.UserRolesList.ToList();
            var newRoles = roles.Where(w => dbRoles.Select(x => x.N).Contains(w.N) && w.Access == false).ToList();
            var deleteRoles = _repository.UserRolesList.Where(w => newRoles.Select(x => x.N).Contains(w.N)).ToList();
            return deleteRoles;
        }
    }
}
