﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Pages;
using webQueueUsersDistrict.ViewModel.SearchByLastName;

namespace webQueueUsersDistrict.Models.UsersOperation.SearchUsers
{
    public interface ISearchUserRepositoryModel
    {
        //PagedList<Chin> SearchByLastName(string lastName, QueryOptions options);

        //PagedList<ViewUserChinModel> FeedViewChinsModel(IQueryable<Chin> chins, QueryOptions options);

        //PagedList<ViewSearchUsersModel> ChinsByLastName(string lastName, QueryOptions options);

        //PagedList<ViewUserChinModel> FeedViewChinsModel(IQueryable<Chin> chins, QueryOptions options);
        PagedList<Chin> FeedViewChinsModel(IQueryable<Chin> chins, QueryOptions options);

        //PagedList<ViewUserChinModel> FeedViewChinsModel(QueryOptions options);
        //PagedList<Chin> ChinByPage(QueryOptions options);

        IQueryable<Chin> ChinByPage();

        IQueryable<Chin> SearchByLastName(string lastName);
        PagedList<ViewUserChinModel> FeedViewChinsModel(PagedList<Chin> pagedUsers, QueryOptions options);


        //PagedList<Chin> FeedViewChinsModel(IQueryable<Chin> chins, QueryOptions options);


        //public PagedList<ViewUserChinModel> FeedViewChinsModel(PagedList<Chin> chins, QueryOptions options);


    }
}
