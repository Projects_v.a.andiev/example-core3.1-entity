﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.Models.Pages;
using webQueueUsersDistrict.ViewModel.SearchByLastName;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Models.UsersOperation.SearchUsers
{
    public class SearchUserRepositoryModel : ISearchUserRepositoryModel
    {
        private UsersContext _context;

        public SearchUserRepositoryModel(UsersContext context)
        {
            _context = context;
        }

        public IQueryable<Chin> SearchByLastName(string lastName)
        {
            var chins = _context
                .Chin
                .Include(x => x.UsersDistrict)
                .Where(w => EF.Functions.Like(w.Fam, $@"%{lastName}%"));

            return chins;
        }


        public IQueryable<Chin> ChinByPage()
        {
            var chins = _context
                .Chin
                .Include(x => x.UsersDistrict);

            return chins;
        }

        //public PagedList<ViewUserChinModel> FeedViewChinsModel(QueryOptions options)
        //{
        //    var chinsModel = new List<ViewUserChinModel>();
        //    foreach (var chin in _context.Chin.Include(x => x.UsersDistrict))
        //    {
        //        chinsModel.Add(new ViewUserChinModel
        //        {
        //            Kod = chin.Kod,
        //            Fam = chin.Fam,
        //            Im = chin.Im,
        //            Otch = chin.Otch,
        //            Pomesh = chin.Pomesh,
        //            PriemDay = chin.PriemDay,
        //            Telef = chin.Telef,
        //            Dolgn = chin.Dolgn,
        //            EmployeeFullName = chin.InspFio,
        //            UserId = (chin.UsersDistrict.Count() == 0 ? 0 : chin.UsersDistrict.FirstOrDefault().UserId)
        //        });
        //    }
        //    return new PagedList<ViewUserChinModel>(chinsModel, options);
        //}

        public PagedList<Chin> FeedViewChinsModel(IQueryable<Chin> chins, QueryOptions options)
        {
           return  new PagedList<Chin>(chins, options);
        }

        public PagedList<ViewUserChinModel> FeedViewChinsModel(PagedList<Chin> chins, QueryOptions options)
        {
            var chinsModel = new List<ViewUserChinModel>();

            foreach (var chin in chins)
            {
                chinsModel.Add(new ViewUserChinModel
                {
                    Kod = chin.Kod,
                    Fam = chin.Fam,
                    Im = chin.Im,
                    Otch = chin.Otch,
                    Pomesh = chin.Pomesh,
                    PriemDay = chin.PriemDay,
                    Telef = chin.Telef,
                    Dolgn = chin.Dolgn,
                    EmployeeFullName = chin.InspFio,
                    UserId = (chin.UsersDistrict.Count() == 0 ? 0 : chin.UsersDistrict.FirstOrDefault().UserId)
                });
            }
            var newPagedList = new PagedList<ViewUserChinModel>(chinsModel, chins.CurrentPage, chins.PageSize, chins.TotalPages, options);

            return newPagedList;
        }

    }
}






