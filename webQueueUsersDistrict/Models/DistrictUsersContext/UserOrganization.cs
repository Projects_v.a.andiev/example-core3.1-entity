﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class UserOrganization
    {
        public int OrganizationId { get; set; }
        public int UserId { get; set; }
        public int DistrictId { get; set; }
        public string Mac { get; set; }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string WindowsVersion { get; set; }
        public string OfficeVersion { get; set; }
        public string NetVersion { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ChangeDate { get; set; }
    }
}
