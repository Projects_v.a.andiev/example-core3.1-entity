﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class UserRoles
    {
        public int N { get; set; }
        public int Users { get; set; }
        public int Role { get; set; }
        public int? RoleCopy { get; set; }

        public virtual Role RoleNavigation { get; set; }
        public virtual UsersDistrict UsersNavigation { get; set; }
    }
}
