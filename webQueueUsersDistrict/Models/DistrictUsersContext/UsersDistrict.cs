﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class UsersDistrict
    {
        public UsersDistrict()
        {
            AccessUserToDistrict = new HashSet<AccessUserToDistrict>();
            UserRoles = new HashSet<UserRoles>();
        }

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int? KodChin { get; set; }
        public string MsNetVersion { get; set; }
        public int StatusUser { get; set; }

        public virtual Chin KodChinNavigation { get; set; }
        public virtual ICollection<AccessUserToDistrict> AccessUserToDistrict { get; set; }
        public virtual ICollection<UserRoles> UserRoles { get; set; }
    }
}
