﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class AccessUserToDistrict
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int AccessDistrict { get; set; }
        public int DistrictId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateChange { get; set; }

        public virtual UsersDistrict User { get; set; }

        public virtual Spr02 AccessUserToDistrictSpr02Navigation { get; set; }
    }
}
