﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class GetDecodingUsers
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int? KodChin { get; set; }
        public string MsNetVersion { get; set; }
        public int StatusUser { get; set; }
        public int Kod { get; set; }
        public string Dolgn { get; set; }
        public string InspFio { get; set; }
        public short? Pomesh { get; set; }
        public string PriemDay { get; set; }
        public string Telef { get; set; }
        public DateTime? TimeIns { get; set; }
        public string Fam { get; set; }
        public string Im { get; set; }
        public string Otch { get; set; }
    }
}
