﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public static class SPCodingDecodingValue
    {
        [DbFunction("Kodirovanie", "dbo")]
        public static string CodingValue(string value) { throw new Exception(); }

        [DbFunction("Raskodirovanie", "dbo")]
        public static string DeCodingValue(string value) { throw new Exception(); }
    }

}
