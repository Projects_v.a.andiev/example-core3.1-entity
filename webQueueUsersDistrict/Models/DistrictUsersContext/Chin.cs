﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class Chin
    {
        public Chin()
        {
            UsersDistrict = new HashSet<UsersDistrict>();
        }

        public int Kod { get; set; }
        public string Dolgn { get; set; }
        public string InspFio { get; set; }
        public short? Pomesh { get; set; }
        public string PriemDay { get; set; }
        public string Telef { get; set; }
        public DateTime? TimeIns { get; set; }
        public string Fam { get; set; }
        public string Im { get; set; }
        public string Otch { get; set; }

        public virtual ICollection<UsersDistrict> UsersDistrict { get; set; }
    }
}
