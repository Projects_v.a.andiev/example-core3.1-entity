﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class Spr02
    {
        public Spr02()
        {
            Spr02AccessToDistrictNavigation =  new HashSet<AccessUserToDistrict>();
        }

        public int Kod { get; set; }
        public string FullName { get; set; }
        public int? PomDel { get; set; }
        public short? KodIps { get; set; }
        public string NameIps { get; set; }
        public int? KodG { get; set; }
        public int? KodGbr { get; set; }
        public string BdName { get; set; }
        public string RosreestrName { get; set; }
        public string RodPadeg { get; set; }
        public int AccessToBase { get; set; }
        public string AccessReturnTime { get; set; }
        
        public virtual ICollection<AccessUserToDistrict> Spr02AccessToDistrictNavigation{ get; set; }
}
}
