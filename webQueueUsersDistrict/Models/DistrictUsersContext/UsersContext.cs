﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class UsersContext : DbContext
    {


        public UsersContext()
        {
        }

        public UsersContext(DbContextOptions<UsersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessUserToDistrict> AccessUserToDistrict { get; set; }
        public virtual DbSet<Chin> Chin { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Spr02> Spr02 { get; set; }
        public virtual DbSet<UserOrganization> UserOrganization { get; set; }
        public virtual DbSet<UserRoles> UserRoles { get; set; }
        public virtual DbSet<UsersDistrict> UsersDistrict { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=SPR;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDbFunction(() => SPCodingDecodingValue.CodingValue(default(string)));

            modelBuilder
                .HasDbFunction(() => SPCodingDecodingValue.DeCodingValue(default(string)));

            modelBuilder.Entity<GetDecodingUsers>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("GetDecodingUsers");

                entity.Property(e => e.Dolgn).HasMaxLength(150);

                entity.Property(e => e.Fam)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Im)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InspFio)
                    .HasColumnName("InspFIO")
                    .HasMaxLength(20);

                entity.Property(e => e.Kod).HasColumnName("kod");

                entity.Property(e => e.Login)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MsNetVersion)
                    .IsRequired()
                    .HasColumnName("ms_net_version")
                    .HasMaxLength(255);

                entity.Property(e => e.Otch)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PriemDay).HasMaxLength(250);

                entity.Property(e => e.Telef).HasMaxLength(10);

                entity.Property(e => e.TimeIns).HasColumnType("smalldatetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<AccessUserToDistrict>(entity =>
                {
                    entity.HasIndex(e => e.UserId);

                    entity.Property(e => e.Id).HasColumnName("ID");

                    entity.Property(e => e.AccessDistrict).HasDefaultValueSql("((1))").ValueGeneratedNever();

                    entity.Property(e => e.DateChange).HasColumnType("datetime");

                    entity.Property(e => e.DateCreate)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                    entity.Property(e => e.UserId).HasColumnName("UserID");

                    entity.HasOne(d => d.User)
                        .WithMany(p => p.AccessUserToDistrict)
                        .HasForeignKey(d => d.UserId)
                        .OnDelete(DeleteBehavior.ClientSetNull);

                    entity.HasOne(x => x.AccessUserToDistrictSpr02Navigation)
                        .WithMany(x => x.Spr02AccessToDistrictNavigation)
                        .HasForeignKey(k => k.DistrictId);
                });

            modelBuilder.Entity<Chin>(entity =>
            {
                entity.HasKey(e => e.Kod)
                    .HasName("PK_CHIN_KOD");

                entity.Property(e => e.Kod).HasColumnName("kod");

                entity.Property(e => e.Dolgn).HasMaxLength(150);

                entity.Property(e => e.Fam)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Im)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InspFio)
                    .HasColumnName("InspFIO")
                    .HasMaxLength(20);

                entity.Property(e => e.Otch)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PriemDay).HasMaxLength(250);

                entity.Property(e => e.Telef).HasMaxLength(10);

                entity.Property(e => e.TimeIns).HasColumnType("smalldatetime");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Role1)
                    .HasColumnName("Role")
                    .HasMaxLength(35);
            });

            modelBuilder.Entity<Spr02>(entity =>
            {
                entity.HasKey(e => e.Kod);

                entity.ToTable("SPR_02");

                entity.Property(e => e.Kod)
                    .HasColumnName("KOD")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccessReturnTime)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.BdName)
                    .HasColumnName("BD_Name")
                    .HasMaxLength(30);

                entity.Property(e => e.FullName)
                    .HasColumnName("FULL_NAME")
                    .HasMaxLength(30);

                entity.Property(e => e.KodG).HasColumnName("KOD_G");

                entity.Property(e => e.KodGbr).HasColumnName("KodGBR");

                entity.Property(e => e.KodIps).HasColumnName("KOD_IPS");

                entity.Property(e => e.NameIps)
                    .HasColumnName("NAME_IPS")
                    .HasMaxLength(30);

                entity.Property(e => e.PomDel).HasColumnName("POM_DEL");

                entity.Property(e => e.RodPadeg).HasMaxLength(255);

                entity.Property(e => e.RosreestrName)
                    .HasColumnName("rosreestr_name")
                    .HasMaxLength(100);


            });

            modelBuilder.Entity<UserOrganization>(entity =>
            {
                entity.HasKey(e => new { e.OrganizationId, e.UserId, e.DistrictId, e.Mac })
                    .HasName("PK_UsersSettings");

                entity.Property(e => e.OrganizationId).HasColumnName("OrganizationID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.DistrictId).HasColumnName("DistrictID");

                entity.Property(e => e.Mac)
                    .HasColumnName("MAC")
                    .HasMaxLength(255);

                entity.Property(e => e.ChangeDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HostName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.NetVersion)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.OfficeVersion)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.WindowsVersion)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<UserRoles>(entity =>
            {
                entity.HasKey(e => e.N)
                    .HasName("PK_UserRoles_N");

                entity.HasIndex(e => e.Role);

                entity.HasIndex(e => e.Users)
                    .HasName("IX_UserRoles_User");

                entity.HasOne(d => d.RoleNavigation)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.Role)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.UsersNavigation)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.Users)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UsersDistrict>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_UsersDistrict_UserID");

                entity.HasIndex(e => e.KodChin)
                    .HasName("IX_UsersDistrict_KodChin_Kod");

                entity.HasIndex(e => e.Login)
                    .HasName("UNIQUE_USERS_LOGIN")
                    .IsUnique();

                entity.HasIndex(e => e.UserId)
                    .HasName("UNIQUE_USERS_USERID")
                    .IsUnique();

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Login).HasMaxLength(50);

                entity.Property(e => e.MsNetVersion)
                    .IsRequired()
                    .HasColumnName("ms_net_version")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('Не определено')");

                entity.Property(e => e.Password).HasMaxLength(20);

                entity.HasOne(d => d.KodChinNavigation)

                    .WithMany(p => p.UsersDistrict)
                    .HasForeignKey(d => d.KodChin)
                    .HasConstraintName("FK_UsersDistrict_Chin_KodChin_Kod")
                    .IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
