﻿using System;
using System.Collections.Generic;

namespace webQueueUsersDistrict.Models.DistrictUsersContext
{
    public partial class Role
    {
        public Role()
        {
            UserRoles = new HashSet<UserRoles>();
        }

        public int Id { get; set; }
        public string Role1 { get; set; }

        public virtual ICollection<UserRoles> UserRoles { get; set; }
    }
}
