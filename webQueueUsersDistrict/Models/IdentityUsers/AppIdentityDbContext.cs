﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.Models.IdentityUsers
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options) : base(options)
        {

        }

        public static async Task CreateAdminAccount(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            UserManager<AppUser> userManager = serviceProvider.GetRequiredService<UserManager<AppUser>>();
            RoleManager<IdentityRole> roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
     
            var newUsers = NewUsers(configuration);

            foreach (var newUser in newUsers)
            {
                if (await userManager.FindByNameAsync(newUser.username) == null)
                {
                    if (await roleManager.FindByNameAsync(newUser.role) == null)
                    {
                        await roleManager.CreateAsync(new IdentityRole(newUser.role));
                    }
                    AppUser user = new AppUser
                    {
                        UserName = newUser.username,
                        Email = newUser.email
                    };

                    IdentityResult result = await userManager.CreateAsync(user, newUser.password);
                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(user, newUser.role);
                    }
                }
            }
        }

        public static List<TestUsers> NewUsers(IConfiguration configuration)
        {
            var testUsers = configuration.GetSection("AdminUser").GetChildren()
           .ToList()
           .Select(x => new TestUsers
           {
               username = x.GetValue<string>("Name"),
               email = x.GetValue<string>("Email"),
               password = x.GetValue<string>("Password"),
               role = x.GetValue<string>("Role"),
           });

            return testUsers.ToList();
        }
    }
}
