﻿namespace webQueueUsersDistrict.Models.IdentityUsers
{
    public class TestUsers
    {
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string role { get; set; }
    }
}