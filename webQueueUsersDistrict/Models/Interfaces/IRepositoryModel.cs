﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.Pages;
using webQueueUsersDistrict.ViewModel.SearchByLastName;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Models.Interfaces
{
    public interface IRepositoryModel
    {
        string CodingValue(string value);
        List<Spr02> Districts { get; }
        List<UserRoles> UserRolesList { get; }

        List<Role> Roles { get; }

        List<UserRoles> UserRolesFull();
        void AddAccessUser(List<ViewAccessUserModel> addsAccess, List<ViewAccessUserModel> accessUserToDistricts);
        List<AccessUserToDistrict> UpdatedRecords(List<ViewAccessUserModel> updateAccess);
        void UpdateAccessUser(List<AccessUserToDistrict> changeAccess, List<ViewAccessUserModel> updateAccess);
        //void RemoveAccessUser(List<ViewAccessUserModel> updateAccess);
        void AddRoleUser(List<UserRoles> updateRoles);
        UsersDistrict UsersDistrictByLogin(string mail);
        void UpdateUsers(ViewUserModel updateUserModel);
        void UpdateChin(ViewChinModel updateChinModel);
        void DeleteRoleUser(List<UserRoles> deleteRoles);
        List<Chin> ChinsByLastName(string lastName);

        List<Chin> ChinsById(int id);

        List<UsersDistrict> UsersDistrictById(int id);

        List<AccessUserToDistrict> AccessUserToDistrictsByUserId(int id);
        UsersDistrict AddNewUsers(ViewEditUserModel newUserModel);

    }
}
