using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using webQueueUsersDistrict.Helpers;
using webQueueUsersDistrict.Infrastructure;
using webQueueUsersDistrict.Infrastructure.CustomAttribute.Users;
using webQueueUsersDistrict.Models;
using webQueueUsersDistrict.Models.DistrictUsersContext;
using webQueueUsersDistrict.Models.IdentityUsers;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.Models.UsersOperation.SearchUsers;

namespace webQueueUsersDistrict
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UsersContext>(option => option.UseSqlServer(Configuration["Data:UsersDbContext:ConnectionString"]), ServiceLifetime.Transient);

            services.AddDbContext<AppIdentityDbContext>(option => option.UseSqlServer(Configuration["DbIdentityUsers:IdentityUsers:ConnectionString"]), ServiceLifetime.Transient);

            services.AddTransient(x => new ValidationAttributeLogin());

            services.AddTransient<IRepositoryModel, RepositoryModel>();

            services.AddTransient<ISearchUserRepositoryModel, SearchUserRepositoryModel>();

            services.AddTransient<IPasswordValidator<AppUser>, CustomPasswordValidator>();

            services.AddTransient<IUserValidator<AppUser>, CustomUserValidator>();

            services.AddSingleton<IClaimsTransformation, LocationClaimsProvider>();

            services.AddTransient<IAuthorizationHandler, BlockUsersHandler>();

            services.AddTransient<IAuthorizationHandler, DocumentAuthorizationHandler>();



            ///�������� �������� �����������
            services.AddAuthorization(opts =>
            {
                opts.AddPolicy("DCUsers", policy =>
                {
                    policy.RequireRole("Users");
                    policy.RequireClaim(ClaimTypes.StateOrProvince, "DC");

                });

                opts.AddPolicy("NotBob", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.AddRequirements(new BlockUsersRequirement("Bob"));
                });


                opts.AddPolicy("AuthorsAndEditors", policy =>
                {
                    policy.AddRequirements(new DocumentAuthorizationRequirement
                    {
                        AllowAuthors = true,
                        AllowEditors = true
                    });
                });

            });

            services.AddAuthentication().AddGoogle(opts =>
            {
                opts.ClientId = "��� ����� ���� ��� ����.apps.googleusercontent.com";
                opts.ClientSecret = "��� ����� ���� ��� ����";
            });


            //services.AddDbContext<AppIdentityDbContext>(options =>
            //options.UseSqlServer(
            //Configuration["Data:SportStoreidentity:ConnectionString"]));

            services.AddIdentity<AppUser, IdentityRole>(opts =>
            {
                opts.User.RequireUniqueEmail = true;
                //opts.User.AllowedUserNameCharacters = "zxcvbnmmmqwertyuiopasdfghjkl";
                opts.Password.RequiredLength = 6;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = false;
                opts.Password.RequireUppercase = false;
                opts.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<AppIdentityDbContext>()
            .AddDefaultTokenProviders();
            services.AddMvc();

            services.ConfigureApplicationCookie(opts =>
                opts.LoginPath = "/Account/Login");


            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStatusCodePages();
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseAuthentication();

            //�������� ��������� ������� ������ �� ����� ������������. ��� ������ ������ � ����������
            AppIdentityDbContext.CreateAdminAccount(app.ApplicationServices, Configuration).Wait();


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action}/{id?}");

                //endpoints.MapControllerRoute(
                //    name: "default",
                //    pattern: "{controller=Home}/{action=EditUserById}/{userId?}");
            });
        }
    }
}
