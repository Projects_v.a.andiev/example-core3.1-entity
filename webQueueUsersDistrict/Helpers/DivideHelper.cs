﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.Helpers
{
    public  class DivideHelper
    {
        public  int DivRoundUp(int dividend, int divisor)
        {
            if (divisor == 0) throw new ArgumentNullException();
            if (divisor == -1 && dividend == Int32.MinValue) throw new Exception();

            int roundedTowardsZeroQuotient = dividend / divisor;
            bool dividedEvenly = (dividend % divisor) == 0;

            if (dividedEvenly)
                return roundedTowardsZeroQuotient;

            bool wasRoundedDown = ((divisor > 0) == (dividend > 0));
            if (wasRoundedDown)
                return roundedTowardsZeroQuotient + 1;
            else
                return roundedTowardsZeroQuotient;
        }
    }
}
