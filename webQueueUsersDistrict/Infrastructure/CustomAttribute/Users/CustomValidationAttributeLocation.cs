﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace webQueueUsersDistrict.Infrastructure
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, Inherited = true)]
    public class CustomValidationAttributeLocation : ValidationAttribute
    {

        private const string DefaultErrorMessage =
        "Можно вводить только числа в название помещения!";
        public override bool IsValid(object value)
        {
            if (value == null) { return false; }

            Regex regex = new Regex(@"([0-9]+)");
            var result = regex.Match(value.ToString()).Success;
            return result;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
              DefaultErrorMessage, name);
        }
        

    }
}
