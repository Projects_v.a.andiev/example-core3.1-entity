﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using webQueueUsersDistrict.Models;
using webQueueUsersDistrict.Models.IdentityUsers;
using webQueueUsersDistrict.Models.Interfaces;
using webQueueUsersDistrict.ViewModel.Update;

namespace webQueueUsersDistrict.Infrastructure.CustomAttribute.Users
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, Inherited = true)]
    public class ValidationAttributeLogin: ValidationAttribute
    {
     
        public IRepositoryModel _repository { get; set; }

        public ValidationAttributeLogin()
        {
            Initialize();
        }

        private void Initialize()
        {
            _repository = new RepositoryModel(new Models.DistrictUsersContext.UsersContext());
        }

        private readonly string _comparisonProperty;

        public ValidationAttributeLogin(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        private const string DefaultErrorMessage =
        @"Введите другой логин!";
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = DefaultErrorMessage;
            var property = (ViewUserModel)(validationContext.ObjectInstance);

            if (value == null) { return new ValidationResult(ErrorMessage); }
            var user = _repository.UsersDistrictByLogin(value.ToString());

            if ( (user != null && property.UserId > 0 && property.Login == user.Login))
            {
                return ValidationResult.Success;
            }
            else if (user == null )
            {
                return ValidationResult.Success;
            }
            else
            {
              return  new ValidationResult(ErrorMessage);
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
              DefaultErrorMessage, name);
        }
    }
}
